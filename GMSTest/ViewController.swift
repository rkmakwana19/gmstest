//
//  ViewController.swift
//  GMSTest
//
//  Created by Rashmikant Makwana on 03/06/20.
//  Copyright © 2020 Rashmikant Makwana. All rights reserved.
//

import UIKit
import GoogleMaps

class ViewController: UIViewController {
    
    var locations: [CLLocationCoordinate2D]!
    
    let arrow = GMSMarker()
    

    @IBOutlet weak var mapView: GMSMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        locations = getLocations()
        
        setup()
        drawPaths()
        
    }
    
    func setup() {
        let camera = GMSCameraPosition.camera(withLatitude: -33.80134526637856, longitude: 150.9692857414484, zoom: 12.0)
        
        mapView.camera = camera
        
    }
    
    func drawPaths() {
        let path = GMSMutablePath()
        for point in locations {
            path.add(point)
        }
        
        let line = GMSPolyline(path: path)
        line.map = mapView
        
        arrow.icon = GMSMarker.markerImage(with: UIColor.green)
        let startPoint = CLLocationCoordinate2D(latitude: -33.79601956850126,
        longitude: 150.93907333910465)
        arrow.position = startPoint
        arrow.map = mapView
    }
    
    func getLocations() -> [CLLocationCoordinate2D] {
        let lats = [-33.79601956850126, -33.8163693868262, -33.818270788649954, -33.79478331282409, -33.827017639561696, -33.861520573831214]
        
        let longs = [150.93907333910465, 150.93438148498535, 150.96276260912418, 150.98278999328613, 151.0055636242032, 150.97638119012117]
        
        var loc = [CLLocationCoordinate2D]()
        for (index, element) in lats.enumerated() {
            let point = CLLocationCoordinate2D(latitude: element,
                                               longitude: longs[index])
            loc.append(point)
            
        }
        return loc
    }

    @IBAction func play(_ sender: Any) {
        animateMarker(pathIndex: 0)
    }
    
    func animateMarker(pathIndex: Int) {
        CATransaction.begin()
        CATransaction.setAnimationDuration(2.0)
        arrow.position = locations[pathIndex+1]
        CATransaction.commit()
        
        DispatchQueue.global(qos: .background).async {
            usleep(useconds_t(1000000 * 2))
            DispatchQueue.main.async{
                if pathIndex < self.locations.count-2 {
                    self.animateMarker(pathIndex: pathIndex+1)
                } else {
                    self.arrow.position = self.locations[0]
                }
            }
        }
    }
    
}

extension ViewController: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        print("TAPPED HERE : \(coordinate.latitude), \(coordinate.longitude)")
    }
    
}
